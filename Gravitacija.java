import java.util.*;

class Gravitacija {
    public static void main (String[] args){
        System.out.println("OIS je zakon!");
        Scanner sc = new Scanner(System.in);
        double visina = sc.nextInt();
        Izpis(visina, pospesek(visina));
        //Izpis(5,6);
    }
    
    
    
    public static double pospesek(double visina){
        double C = 6.674 * Math.pow(10,-11);    // 0.00000000006674;         
        double M = 5.972 * Math.pow(10,24);     // 5792000000000000000000000;   
        double r = 6.371 * Math.pow(10,6);      // 6371000
        return (C*M)/Math.pow((r+visina),2);
    }

    public static void Izpis(double visina, double pospesek){
        System.out.println("Na nadmorski višini " + visina + " m je gravitacijski pospešek " + pospesek +" m/s^2.");

    }
    
    
}